Rails.application.routes.draw do
  devise_for :users, :controllers => { :registrations => "users", sessions: "users/sessions" }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get "sales_and_repairs/index"
  get 'products/index'
  get "account/index"


  root :to => "sales_and_repairs#index", :constraints => lambda { |request| request.env['warden'].user.nil? }, as: :unauthenticated_root
  root :to => 'sales_and_repairs#index', :constraints => lambda { |request| request.env['warden'].user.role == ('proprietor' || 'developer') if request.env['warden'].user }, as: :admin_root
  root :to => 'products#index', :constraints => lambda { |request| request.env['warden'].user.role == 'stock_custodian' if request.env['warden'].user }, as: :stock_custodian_root
  root :to => 'accounting/accounts#index', :constraints => lambda { |request| request.env['warden'].user.role == 'bookkeeper' if request.env['warden'].user }, as: :bookkeeper_root

  resources :products do
    get :autocomplete_product_name_and_description, on: :collection
    match "/scope_to_category" => "products#scope_to_category", as: :scope_to_category, via: [:get], on: :collection
    resources :stocks, module: :products
    resources :barcodes, module: :products
    match "/cash_sales" => "products#cash_sales", as: :cash_sales, via: [:get], on: :member
    match "/credit_sales" => "products#credit_sales", as: :credit_sales, via: [:get], on: :member
    match "/stock_histories" => "products#stock_histories", as: :stock_histories, via: [:get], on: :member
    match "/barcode" => "products#barcode", as: :barcode, via: [:get], on: :member
    match "/reports" => "products#reports", as: :reports, via: [:get], on: :member
    match "/scope_to_date_cash_sales" => "products#scope_to_date_cash_sales",  via: [:get], on: :member
    match "/scope_to_date_credit_sales" => "products#scope_to_date_credit_sales",  via: [:get], on: :member
    match "/scope_to_date_sales" => "products#scope_to_date_sales",  via: [:get], on: :member
    collection { post :import}
    match "/download" => "products#download", as: :download, via: [:get], on: :collection
    match "/export" => "products#export", as: :export, via: [:get], on: :collection
  end
  resources :barcodes do
    collection { post :import}
  end

  resources :credit_collections do
    match "/scope_to_date" => "credit_collections#scope_to_date",  via: [:get], on: :collection
  end

  resources :office_expenses do
    match "/scope_to_date" => "office_expenses#scope_to_date",  via: [:get], on: :collection
  end

  resources :line_items do
    resources :credit_payments, only: [:new, :create], module: :accounting
    match "/return_line_item" => "line_items#return_line_item", as: :return_line_item, via: [:get, :post]
    match "/return" => "line_items#return", as: :return, via: [:get, :post]
  end

  resources :carts
  resources :orders do
    resources :sales_returns, only: [:destroy], module: :orders
    match "/guest" => "orders#guest",  via: [:post], on: :member
    match "/print" => "orders#print",  via: [:get], on: :member
    match "/print_invoice" => "orders#print_invoice",  via: [:get], on: :member
    match "/print_official_receipt" => "orders#print_official_receipt",  via: [:get], on: :member
    match "/scope_to_date" => "orders#scope_to_date",  via: [:get], on: :collection
    match "/scope_to_date_summary" => "orders#scope_to_date_summary",  via: [:get], on: :collection
    match "/catering" => "orders#catering", as: :catering, via: [:get], on: :collection
    match "/sales_returns" => "orders#sales_returns", as: :sales_returns, via: [:get], on: :collection
    match "/return" => "orders#return", as: :return, via: [:get, :post]
    match "/sales_and_repairs" => "orders#sales_and_repairs", as: :sales_and_repairs, via: [:get], on: :collection
  end

  resources :members, only: [:new, :create, :edit, :update]
  resources :organizations, only: [:new, :create, :edit, :update]
  resources :customers do
    collection { post :import}
    get :autocomplete_customer_full_name, on: :collection
    resources :payments, only: [:new, :create], module: :customers
    resources :interests, only: [:new, :create], module: :customers

    match "/info" => "customers#info", as: :info, via: [:get], on: :member
    match "/purchases" => "customers#purchases", as: :purchases, via: [:get], on: :member
    match "/account_details" => "customers#account_details", as: :account_details, via: [:get], on: :member
    match "/payments" => "customers#payments", as: :payments, via: [:get], on: :member

    resources :line_items, only: [:index], module: :customers do
      match "/scope_to_date" => "line_items#scope_to_date",  via: [:get], on: :collection, module: :customers
    end

    match "/export" => "customers#export", as: :export, via: [:get], on: :collection
  end
  
  resources :payables, only: [:index]
  resources :suppliers, :except => [:destroy] do
    get :autocomplete_supplier_owner, on: :collection
    resources :payments, only: [:new, :create], module: :suppliers
    match "/cash_stocks" => "suppliers#cash_stocks", as: :cash_stocks, via: [:get], on: :member
    match "/credit_stocks" => "suppliers#credit_stocks", as: :credit_stocks, via: [:get], on: :member
    match "/credit_payments" => "suppliers#credit_payments", as: :credit_payments, via: [:get], on: :member

  end
  resources :reports, only: [:index]
  resources :settings, only: [:index]
  resources :credits, only: [:index]
  namespace :accounts_receivables do
    resources :interests, only:[:create]
  end
  resources :available_products, only: [:index]

  resources :low_stock_products, only: [:index]
  resources :out_of_stock_products, only: [:index]
  resources :expired_products, only: [:index]


  resources :refunds, only: [:index, :new, :create]
  resources :taxes, only: [:new, :create]
  resources :stocks, only: [:index, :show, :new, :create, :destroy] do
    collection { post :import}
    get :autocomplete_stock_name, on: :collection
    match "/scope_to_date" => "stocks#scope_to_date",  via: [:get], on: :collection
    resources :refunds, only: [:new, :create], module: :stocks
    match "/out_of_stock" => "stocks#out_of_stock", via: [:get], on: :collection
    match "/returned" => "stocks#returned", via: [:get], on: :collection
    match "/discontinued" => "stocks#discontinued", via: [:get], on: :collection
    match "/discontinue" => "stocks#discontinue", as: :discontinue, via: [:get, :post]
    match "/continue" => "stocks#continue", as: :continue, via: [:get, :post]
    match "/export" => "stocks#export", as: :export, via: [:get], on: :collection
  end
  resources :users, only: [:show, :edit, :update] do 
    match "/info" => "users#info", as: :info, via: [:get], on: :member
    match "/activities" => "users#activities", as: :activities, via: [:get], on: :member
  end
  resources :employees, only: [:new, :create]
  resources :accounting, only: [:index] do
    get :autocomplete_account_name, on: :collection
  end
  namespace :accounting do
    resources :balance_sheets, only:[:index, :scope_to_date] do
      match "/scope_to_date" => "balance_sheets#scope_to_date", via: [:get], on: :collection
    end
    resources :income_statements, only:[:index, :scope_to_date] do
      match "/scope_to_date" => "income_statements#scope_to_date", via: [:get], on: :collection
    end
    resources :reports, only:[:index]
    resources :accounts do
      get :autocomplete_accounting_account_name, on: :collection
      match "/activate" => "accounts#activate", via: [:post], on: :member
      match "/deactivate" => "accounts#deactivate", via: [:post], on: :member
    end
    resources :assets, controller: 'accounts', type: 'Accounting::Asset'
    resources :liabilities, controller: 'accounts', type: 'Accounting::Liability'
    resources :equities, controller: 'accounts', type: 'Accounting::Equity'
    resources :revenues, controller: 'accounts', type: 'Accounting::Revenue'
    resources :expenses, controller: 'accounts', type: 'Accounting::Expense'
    resources :member_entries, only: [:new, :create]
    resources :fund_transfers, only: [:new, :create]
    resources :remittances, only: [:new, :create]
    resources :supplier_entries, only: [:new, :create]
    resources :sales_entries, only: [:new, :create]
    resources :expense_entries, only: [:new, :create]

    resources :entries do
      get :autocomplete_entry_description, on: :collection
      match "/scope_to_date" => "entries#scope_to_date", as: :scope_to_date, via: [:get], on: :collection
    end
  end

  namespace :owner do
    resources :dashboard, only: [:index]
  end
  resources :sales_returns, only: [:new, :destroy]

  resources :service_fees, module: :service_charges
  resources :service_charges
end
