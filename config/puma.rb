#!/usr/bin/env puma
environment ENV['RAILS_ENV'] || 'production'
daemonize true
pidfile "/var/www/gentech_ict/shared/tmp/pids/puma.pid"
stdout_redirect "/var/www/gentech_ict/shared/tmp/log/stdout", "/var/www/gentech_ict/shared/tmp/log/stderr"
threads 0, 16
bind "unix:///var/www/gentech_ict/shared/tmp/sockets/puma.sock"
