class CreateServiceCharges < ActiveRecord::Migration[5.1]
  def change
    create_table :service_charges do |t|
			t.integer :cart_id
      t.string :description
      t.string :unit
      t.decimal :quantity
      t.decimal :unit_cost
      t.decimal :total_cost
      t.integer :charge_type
      t.integer :order_id, index: true, foreign_key: true
      t.integer :customer_id, index: true, foreign_key: true
      t.integer :user_id, index: true

      t.timestamps
    end
  end
end
