class AddFreightAmountToStocks < ActiveRecord::Migration[5.1]
  def change
  	add_column :stocks, :freight_amount, :decimal, default: "0.0"
  end
end
