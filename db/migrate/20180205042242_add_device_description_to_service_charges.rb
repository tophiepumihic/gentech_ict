class AddDeviceDescriptionToServiceCharges < ActiveRecord::Migration[5.1]
  def change
  	add_column :service_charges, :device_description, :string
  end
end
