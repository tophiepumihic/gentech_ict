class CreateFreightIns < ActiveRecord::Migration[5.1]
  def change
    create_table :freight_ins do |t|
    	t.decimal :amount,          default: "0.0"
      t.integer :stock_id
      t.integer :employee_id

      t.timestamps
    end
    add_index :freight_ins, :employee_id
  end
end
