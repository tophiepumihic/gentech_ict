class CreateBarcodes < ActiveRecord::Migration[5.1]
  def change
    create_table :barcodes do |t|
      t.string :code
      t.belongs_to :product, foreign_key: true, index: true

      t.timestamps
    end
  end
end
