class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :name
      t.string :description
      t.string :unit
      t.decimal :retail_price
      t.decimal :stock_alert_count, default: 1
      t.string :name_and_description
      t.integer :status
      t.boolean :deleted, default: false

      t.timestamps
    end
    add_index :products, :name
    add_index :products, :description
  end
end
