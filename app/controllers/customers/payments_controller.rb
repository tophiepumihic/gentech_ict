module Customers
  class PaymentsController < ApplicationController
    def new
      @member = Customer.find(params[:customer_id])
      @entry = AccountsReceivablePaymentForm.new
      # authorize @entry
    end

    def create
      @member = Customer.find(params[:customer_id])
      @entry = AccountsReceivablePaymentForm.new(entry_params)
      if @entry.save
        @member.set_has_credit_to_false!
        redirect_to payments_customer_path(@member), notice: "Payment saved successfully."
      else
        render :new
      end
      # authorize @entry
    end

    def show
      @entry = Accounting::Entry.find(params[:id])
    end

    private
    def entry_params
      params.require(:accounting_entry).permit(:date, :description, :reference_number, debit_amounts_attributes:[:amount, :account_id, :id], credit_amounts_attributes:[:amount, :account_id, :id])
    end
  end
end
