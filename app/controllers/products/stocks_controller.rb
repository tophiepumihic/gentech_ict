module Products
  class StocksController < ApplicationController
    def new
      @product = Product.find(params[:product_id])
      @stock = @product.stocks.build
    end

    def create
      @product = Product.find(params[:product_id])
      @stock = @product.stocks.create(stock_params)
      @stock.employee = current_user
      if @stock.save
        redirect_to stock_histories_product_path(@product), notice: "New stock saved successfully."
        @stock.create_entry
        @stock.set_status_to_product
        @stock.purchased!
      else
        render :new
      end
    end

    def edit
      @product = Product.find(params[:product_id])
      @stock = Stock.find(params[:id])
    end

    def update
      @product = Product.find(params[:product_id])
      @stock = @product.stocks.find(params[:id])
      if @stock.update(stock_params)
        @stock.update_entry
        redirect_to stock_histories_product_path(@product), notice: "New stock saved successfully."
      end
    end

    def show
      @product = Product.find(params[:product_id])
      @stock = @product.stocks.find(params[:id])
    end

    def destroy
      @product = Product.find(params[:product_id]) if params[:product_id].present?
      @stock = Stock.find(params[:id])
      if @stock.line_items.blank?
        @stock.destroy
        if request.referer == stock_histories_product_url(@product)
          redirect_to stock_histories_product_path(@product), alert: 'Stock deleted successfully.'
        else
          redirect_to stocks_path, alert: "Stock deleted successfully."
        end
      else
        if request.referer == stock_histories_product_url(@product)
          redirect_to stock_histories_product_path(@stock), alert: "Unable to delete stock because there are orders associated with it. Discontinue instead!"
        else
          redirect_to stocks_path, alert: "Unable to delete stock because there are orders associated with it. Discontinue instead!"
        end
      end
    end
    
    private
    def stock_params
      params.require(:stock).permit(:has_freight, :freight_amount, :payment_type, :supplier_id, :reference_number, :quantity, :date, :total_cost, :unit_cost, :retail_price)
    end
  end
end
