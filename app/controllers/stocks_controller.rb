class StocksController < ApplicationController
  autocomplete :stock, :name, full: true
  
  def index
    if params[:name]
      @stocks = Stock.search_by_name(params[:name]).page(params[:page]).per(50)
      #@stocks = Kaminari.paginate_array(@available).page(params[:page]).per(50)
    else
      @available = Stock.available
      @stocks = Kaminari.paginate_array(@available).page(params[:page]).per(50)
    end
  end

  def import
    begin
      Stock.import(params[:file])
      redirect_to settings_url, notice: 'Stocks Imported'
    rescue
      redirect_to settings_url, notice: 'Invalid CSV File.'
    end
  end

  def export
    @categories = Category.order(:name).all
    @stocks = Stock.available
    respond_to do |format|
      format.xlsx { render xlsx: "export", disposition: 'inline', filename: "StocksExport-#{Time.zone.now.strftime('%B %e, %Y')}" }
    end
  end

  def expired
    @expired = Stock.expired
    @stocks = Kaminari.paginate_array(@expired).page(params[:page]).per(50)
  end

  def out_of_stock
    @out_of_stock = Stock.out_of_stock
    @stocks = Kaminari.paginate_array(@out_of_stock).page(params[:page]).per(50)
  end

  def returned
    @returned = Refund.all
    @stocks = Kaminari.paginate_array(@returned).page(params[:page]).per(50)
  end

  def forwarded
    @forwarded = StockTransfer.all
    @stocks = Kaminari.paginate_array(@forwarded).page(params[:page]).per(50)
  end

  def discontinued
    @discontinued = Stock.discontinued
    @stocks = Kaminari.paginate_array(@discontinued).page(params[:page]).per(50)
  end

  def discontinue
    @stock = Stock.find(params[:stock_id])
    @stock.create_expense_from_expired_stock
    @stock.discontinued!
    redirect_to stocks_path, notice: 'Stock discontinued successfully.'
  end

  def continue
    @stock = Stock.find(params[:stock_id])
    @stock.purchased!
    @stock.remove_expense_from_expired_stock
    redirect_to stocks_path, notice: 'Stock set to active successfully.'
  end

  def new
    @stock = Stock.new
  end

  def create
    @stock = Stock.create(stock_params)
    @stock.employee = current_user
    if @stock.save
      redirect_to stocks_url, notice: "New stock saved successfully."
      if @stock.received?
        @stock.set_stock_status_to_product
        @stock.purchased!
      elsif !@stock.received?
        @stock.create_entry
        @stock.set_stock_status_to_product
        @stock.purchased!
      end
    else
      render :new
    end
  end

  def show
    @stock = Stock.find(params[:id])
  end

  def scope_to_date
    @from_date = params[:from_date] ? Time.parse(params[:from_date]) : Time.zone.now.yesterday.end_of_day
    @to_date = params[:to_date] ? Time.parse(params[:to_date]) : Time.zone.now.end_of_day
    @stocks = Stock.created_between({from_date: @from_date, to_date: @to_date})
    respond_to do |format|
      format.html
      format.pdf do
        pdf = StocksPdf.new(@stocks, @from_date, @to_date, view_context)
          send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Purchases Report.pdf"
      end
    end
  end

  def destroy
    @product = Product.find(params[:product_id]) if params[:product_id].present?
    @stock = Stock.find(params[:id])
    @stock.employee = current_user
    if @stock.line_items.blank?
      if request.referer == product_stock_path(@product)
        @stock.destroy
        #@stock.entry.destroy
        redirect_to stock_histories_product_path, alert: 'Stock deleted successfully.'
      else
        @stock.destroy
        #@stock.entry.destroy
        redirect_to stocks_url, alert: 'Stock deleted successfully.'
      end
    else
      if @product.present? && request.referer == product_stock_path(@product)
        redirect_to stock_histories_product_path(@product), alert: "Unable to delete stock because there are orders associated with it. Discontinue instead!"
      else
        redirect_to stocks_url, alert: "Unable to delete stock because there are orders associated with it. Discontinue instead!"
      end
    end
  end

  private
  def stock_params
    params.require(:stock).permit(:has_freight, :freight_amount, :payment_type, :supplier_id, :reference_number, :product_id, :quantity, :date, :unit_cost, :total_cost, :retail_price, :wholesale_price)
  end
end
