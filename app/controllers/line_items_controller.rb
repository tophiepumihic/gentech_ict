class LineItemsController < ApplicationController
  def create
    @cart = current_cart
    @line_item = @cart.line_items.create(line_item_params)
    @line_item.set_total_price
    respond_to do |format|
      if @line_item.valid?
        @line_item.save!
        # @line_item.retail!
        @line_item.stock.product.set_status
        @cart.add_line_item(@line_item)
        format.html { redirect_to sales_and_repairs_index_url }
        format.js { @current_item = @line_item }
      else
        format.html { redirect_to sales_and_repairs_index_url, notice: @line_item.errors }
      end
    end
  end

  def return
    @order = Order.find(params[:order_id])
    @line_item = LineItem.find(params[:line_item_id])
    @line_item.employee = current_user
    @line_item.return_quantity_to_stock!
    @line_item.stock.product.set_status
    @line_item.create_entry_for_sales_return
    @line_item.destroy
    redirect_to @order, alert: 'Item has been returned.'
  end

  def destroy
    @line_item = LineItem.find(params[:id])
    @line_item.destroy
    redirect_to sales_and_repair_index_url, alert: "Item has been removed."
  end

  private
  def line_item_params
    params.require(:line_item).permit(:customer_id, :stock_id, :quantity, :unit_price, :total_price)
  end
end
