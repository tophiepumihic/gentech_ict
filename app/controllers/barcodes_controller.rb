class BarcodesController < ApplicationController

	def import
    begin
      Barcode.import(params[:file])
      redirect_to settings_url, notice: 'Barcodes Imported'
    rescue
      redirect_to settings_url, notice: 'Invalid CSV File.'
    end
  end
end