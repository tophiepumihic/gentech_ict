class SalesAndRepairsController < ApplicationController

  def index
    if params[:name_and_description].present?
      @products = Product.search_by_name(params[:name_and_description]).pluck(:id)
      if @products.present?
        @stocks = Stock.order(date: :asc).joins(:product).where(products: { id: @products })
      else
        redirect_to caterings_path, notice: 'Product is either out of stock or expired.'
      end
    else
      @stocks = Stock.purchased.all
    end
    authorize :store
    @cart = current_cart
    @line_item = LineItem.new
    @order = Order.new
    @order.build_discount
  end
end
