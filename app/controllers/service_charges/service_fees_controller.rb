module ServiceCharges
  class ServiceFeesController < ApplicationController
    def new
      @cart = current_cart
      @service_charge = @cart.service_charges.build
    end

    def create
      @cart = current_cart
      @service_charge = @cart.service_charges.create(service_charge_params)
    end

    def destroy
      @service_charge = ServiceCharge.find(params[:id])
      @service_charge.destroy
      redirect_to sales_and_repairs_url, alert: "Item has been removed."
    end

    private
    def service_charge_params
      params.require(:service_charge).permit(:device_description, :description, :unit, :quantity, :unit_cost, :total_cost, :charge_type, :user_id)
    end
  end
end