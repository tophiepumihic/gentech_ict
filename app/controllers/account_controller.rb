class AccountController < ApplicationController
  def index
  	@cash_on_hand = Accounting::Account.find_by(name: 'Cash on Hand').balance
  end
end