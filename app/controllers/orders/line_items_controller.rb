module Orders
	class LineItemsController < ApplicationController	
		def return
	    @order = Order.find(params[:order_id])
	    @line_item = LineItem.find(params[:id])
	    @line_item.employee = current_user
	    @line_item.return_quantity_to_stock!
	    @line_item.create_entry_for_sales_return
	    @line_item.destroy
	    redirect_to @order, alert: 'Sales Return saved successfully.'
	  end
	end
end
