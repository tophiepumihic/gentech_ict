class ServiceChargesController < ApplicationController

  def destroy
    @service_charge = ServiceCharge.find(params[:id])
    @service_charge.destroy
    redirect_to sales_and_repairs_index_url, alert: "Item has been removed."
  end

  private
  def service_charge_params
    params.require(:service_charge).permit(:device_description, :description, :unit, :quantity, :unit_cost, :total_cost, :charge_type, :customer_id)
  end
end
