function calculateChange() {
  var totalAmount = document.getElementById('total').value;
  var cashTendered = document.getElementById('order_cash_tendered').value;

  var change = document.getElementById('order_change');
  var myResult = cashTendered - totalAmount;
  change.value = myResult;
}
