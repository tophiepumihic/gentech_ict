class Stock < ApplicationRecord
  include PgSearch
  require 'csv'

  pg_search_scope :search_by_name, :against => [:name]
  enum stock_type:[:purchased, :returned, :discontinued]
  enum payment_type: [:cash, :credit]
  belongs_to :product
  belongs_to :employee
  belongs_to :supplier, optional: true
  has_one :entry, class_name: "Accounting::Entry", foreign_key: 'stock_id', dependent: :destroy
  has_one :freight_in
  has_many :line_items
  has_many :orders, through: :line_items
  has_many :refunds
  before_update :set_prices

  validates :quantity, :supplier_id, :unit_cost, :total_cost,  presence: true, numericality: true
  before_save :set_date, :set_name

  def self.import(file)
    CSV.foreach(file.path, headers: true) do |row|
      stock_hash = row.to_hash
      stock = Stock.where(id: stock_hash['id'])

      if stock.count == 1
        stock.first.update_attributes(stock_hash)
      else
        stock_create = Stock.create!(stock_hash)
        # stock_create.create_entry
        stock_create.set_stock_status_to_product
      end
    end
  end

  def to_s
    name
  end

  def self.returned
    all.order(date: :desc).where(stock_type: 1)
  end

  def self.discontinued
    all.order(date: :desc).where(stock_type: 2)
  end

  def self.total_cost_of_purchase
    all.order(date: :desc).sum(:total_cost)
  end

  def self.created_between(hash={})
    if hash[:from_date] && hash[:to_date]
      from_date = hash[:from_date].kind_of?(Time) ? hash[:from_date] : DateTime.parse(hash[:from_date].strftime('%Y-%m-%d 12:00:00 AM'))
      to_date = hash[:to_date].kind_of?(Time) ? hash[:to_date] : DateTime.parse(hash[:to_date].strftime('%Y-%m-%d 23:59:59 PM'))
      where('date' => from_date..to_date)
    else
      all
    end
  end

  def in_stock
    if self.returned? || self.discontinued?
      0
    else
      quantity - sold
    end
  end

  def sold
    line_items.sum(:quantity)
  end

  def total_cost_less_freight_in
    total_cost - freight_amount
  end

  def total_cost_plus_freight_in
    total_cost
  end

  def self.available
    all.order(date: :desc).where.not(stock_type: [1,2,3]).select{ |a| !a.out_of_stock?}
  end

  def self.out_of_stock
    all.order(date: :desc).where.not(stock_type: [1,2,3]).select{ |a| a.out_of_stock?}
  end

  def low_stock? 
    low_stock? && !returned? && !discontinued?
  end

  def discontinued?
    stock_type == "discontinued"
  end

  def discontinued_or_out_of_stock?
    discontinued? || (discontinued? && out_of_stock?)
  end

  def returned?
    stock_type == "returned"
  end

  def returned_or_out_of_stock?
    returned? || (returned? && out_of_stock?)
  end

  def out_of_stock?
    (in_stock.zero? || in_stock.negative?) && !returned? && !discontinued?
  end

  def low_stock?
    quantity <= product.stock_alert_count && !out_of_stock? && !returned? && !discontinued?
  end

  def has_freight?
    !freight_amount.zero?
  end

  def status
    if out_of_stock?
      "Out of Stock"
    elsif low_stock? && !discontinued?
      "Low on Stock"
    elsif discontinued? || discontinued_or_out_of_stock?
      "Discontinued"
    elsif returned_or_out_of_stock?
      "Returned"
    end
  end

  def set_status_to_product
    if (!out_of_stock? && !low_stock?) || (!out_of_stock? && !low_stock?)
      self.product.available!
    elsif out_of_stock?
      self.product.unavailable!
    end
  end

  def in_stock_amount
    in_stock * unit_cost
  end

  def update_entry
    if entry.present?
      @accounts_payable = Accounting::Account.find_by(name: "Accounts Payable-Trade")
      @merchandise_inventory = Accounting::Account.find_by(name: "Merchandise Inventory")
      @cash_on_hand = Accounting::Account.find_by(name: "Cash on Hand")
      @freight_in = Accounting::Account.find_by(name: "Freight In")
      entry.debit_amounts.last.update!(amount: self.total_cost, account: @merchandise_inventory) 
      entry.credit_amounts.last.update!(amount: self.total_cost, account: @accounts_payable)
    end
  end

  def create_entry
    @merchandise_inventory = Accounting::Account.find_by(name: "Merchandise Inventory")
    @accounts_payable = Accounting::Account.find_by(name: "Accounts Payable-Trade")
    @cash_on_hand = Accounting::Account.find_by(name: "Cash on Hand")
    @freight_in = Accounting::Account.find_by(name: "Freight In")
    #CASH PURCHASE ENTRY
    if self.cash? && !has_freight?
      Accounting::Entry.create!(entry_type: "cash_stock", stock_id: self.id, commercial_document_id: self.supplier.id, 
        commercial_document_type: self.supplier.class, date: self.date, 
        description: "Cash Purchase of stocks", 
        debit_amounts_attributes: [amount: self.total_cost, account: @merchandise_inventory], 
        credit_amounts_attributes:[amount: self.total_cost, account: @cash_on_hand],  
        employee_id: self.employee_id)

    elsif self.cash? && has_freight?
      Accounting::Entry.create!(entry_type: "cash_stock", stock_id: self.id, commercial_document_id: self.supplier.id, 
        commercial_document_type: self.supplier.class, date: self.date, 
        description: "Cash Purchase of stocks with freight in of #{self.freight_amount}", 
        debit_amounts_attributes: [{amount: self.total_cost_less_freight_in, account: @merchandise_inventory}, {amount: self.freight_amount, account: @freight_in}], 
        credit_amounts_attributes:[{amount: self.total_cost_less_freight_in, account: @cash_on_hand}, {amount: self.freight_amount, account: @cash_on_hand}], 
        employee_id: self.employee_id)
      
    # #Cedit PURCHASE ENTRY##
    elsif self.credit? && !has_freight?
      Accounting::Entry.create!(entry_type: "credit_stock", stock_id: self.id, commercial_document_id: self.supplier.id, 
        commercial_document_type: self.supplier.class, date: self.date, 
        description: "Credit Purchase of stocks", 
        debit_amounts_attributes: [amount: self.total_cost, account: @merchandise_inventory], 
        credit_amounts_attributes:[amount: self.total_cost, account: @accounts_payable],  
        employee_id: self.employee_id)

    elsif self.credit? && !discounted? && has_freight?
      Accounting::Entry.create!(entry_type: "credit_stock", stock_id: self.id, commercial_document_id: self.supplier.id, 
        commercial_document_type: self.supplier.class, date: self.date, 
        description: "Credit Purchase of stocks with freight in of #{self.freight_amount}", 
        debit_amounts_attributes: [{amount: self.total_cost_less_freight_in, account: @merchandise_inventory}, {amount: self.freight_amount, account: @freight_in}], 
        credit_amounts_attributes:[{amount: self.total_cost_less_freight_in, account: @accounts_payable}, {amount: self.freight_amount, account: @accounts_payable}],  
        employee_id: self.employee_id)
    end
  end

  private
  def set_prices
    retail_price = (unit_cost / 65) * 100
    self.product.update!(retail_price: retail_price)
  end
  def set_date
    if self.date.nil?
      self.date = Time.zone.now
    end
  end
  def set_name
    self.name = self.product.name_and_description
  end
end
