class Order < ApplicationRecord
  include PgSearch
  pg_search_scope :text_search, :against => [:reference_number],
                  :associated_against => {:customer => [:full_name], :invoice_number => [:number]}

  has_one :official_receipt, as: :receiptable
  has_one :invoice_number, dependent: :destroy
  has_one :entry, class_name: "Accounting::Entry", as: :commercial_document, dependent: :destroy

  belongs_to :employee, foreign_key: 'employee_id'
  belongs_to :customer, foreign_key: 'customer_id'
  enum pay_type:[:cash, :credit]
  enum payment_status: [:paid, :unpaid]
  has_many :line_items, dependent: :destroy
  has_many :service_charges, dependent: :destroy
  has_many :stocks, through: :line_items
  has_one :discount, dependent: :destroy

  scope :sold_on, lambda {|start_date, end_date| where("date >= ? AND date <= ?", start_date, end_date )}
  belongs_to :tax
  before_save :set_date, :set_customer
  accepts_nested_attributes_for :discount
  accepts_nested_attributes_for :entry

  def self.created_between(hash={})
    if hash[:from_date] && hash[:to_date]
      from_date = hash[:from_date].kind_of?(Time) ? hash[:from_date] : DateTime.parse(hash[:from_date].strftime('%Y-%m-%d 12:00:00 AM'))
      to_date = hash[:to_date].kind_of?(Time) ? hash[:to_date] : DateTime.parse(hash[:to_date].strftime('%Y-%m-%d 23:59:59 PM'))
      where('date' => from_date..to_date)
    else
      all
    end
  end

  def self.cost_of_goods_sold
    all.to_a.sum{ |a| a.cost_of_goods_sold }
  end

  def cost_of_goods_sold
    if service_charges.present?
      line_items.cost_of_goods_sold
    else
      line_items.cost_of_goods_sold
    end
  end

  def self.income
    all.to_a.sum{ |a| a.income }
  end

  def income
    if service_charges.present?
      total_amount - cost_of_goods_sold
    else
      line_items.income
    end
  end
  
  def customer_name
    customer.try(:full_name)
  end
  def vatable_amount
    0
  end
  def vat_percentage
    12
  end
  def machine_accreditation
    ""
  end
  def reference_number
    "#{id.to_s.rjust(8, '0')}"
  end
  def self.total_amount_without_discount
    all.map{|a| a.total_amount_without_discount }.sum
  end
  def self.total_amount_less_discount
    all.map{|a| a.total_amount_less_discount }.sum
  end
  def self.total_amount_less_discount_plus_interest
    all.map{|a| a.total_amount_less_discount }.sum + all.line_items.map {|l| l.interest_programs.sum(:amount)}.sum
  end
  def self.total_discount
    all.map{|a| a.total_discount }.sum
  end
  def tax_rate
    if business.non_vat_registered?
      0.03
    elsif business.vat_registered?
      0.12
    end
  end
  
  def total_amount
    if service_charges.present?
      items_amount + repair_cost
    else
      items_amount
    end
  end

  def add_line_items_from_cart(cart)
    cart.line_items.each do |item|
      item.cart_id = nil
      line_items << item
    end
  end

  def add_additional_charges(cart)
    cart.service_charges.each do |item|
      item.cart_id = nil
      service_charges << item
    end
  end

  def items_amount
    line_items.sum(:total_price)
  end

  def repair_cost
    service_charges.sum(:total_cost)
  end

  def items_and_repairs
    line_items + service_charges
  end

  def self.items_and_repairs
    self.select{|o| o.line_items + o.service_charges}
  end

  def stock_cost
    line_items.map{|a| a.stock.unit_cost * a.quantity}.sum
  end

  def return_line_items_to_stock!
    line_items do |line_item|
      line_item.stock.update_attributes!(quantity: line_item.quantity + line_item.stock.quantity)
      line_item.stock.product.set_status
    end
  end

  def remove_order_entry!
    Accounting::Entry.where(order_id: id).destroy_all
  end

  def subscribe_to_program!
    if self.line_items.last.stock.product.program.present?
      ProgramSubscription.create(customer_id: self.customer_id, program_id: self.line_items.last.stock.product.program.id)
    end
  end

  def set_customer_has_credit_to_true!
    Customer.find(customer_id).update(has_credit: true)
  end

  def set_has_credit_to_false!
    Customer.find(customer_id).update(has_credit: false)
  end

  def discontinue_stock_if_zero!
    line_items.each do |line_item|
      if line_item.stock.in_stock == 0
        line_item.stock.update!(stock_type: "discontinued")
      end
    end
  end

  def create_entry
    @cash_on_hand = Accounting::Account.find_by(name: "Cash on Hand")
    @cost_of_goods_sold = Accounting::Account.find_by(name: "Cost of Goods Sold")
    @sales = Accounting::Account.find_by(name: "Sales")
    @repairs_and_maintenance = Accounting::Account.find_by(name: 'Income from Repairs and Maintenance')
    @merchandise_inventory = Accounting::Account.find_by(name: "Merchandise Inventory")
    @accounts_receivable = Accounting::Account.find_by(name: "Accounts Receivables Trade - Current")

    if self.cash?
      entry_for_cash
    elsif self.credit?
      entry_for_credit
    end
  end

  def entry_for_cash
    if self.line_items.present? && self.service_charges.blank?
      Accounting::Entry.create(entry_type: "cash_order", order_id: self.id, commercial_document_id: self.customer_id, 
      commercial_document_type: self.customer.class, date: self.date, 
      description: "Sales for order ##{self.reference_number}", 
      debit_amounts_attributes: [{amount: self.items_amount, account: @cash_on_hand}, 
        {amount: self.stock_cost, account: @cost_of_goods_sold}], 
      credit_amounts_attributes:[{amount: self.items_amount, account: @sales}, 
        {amount: self.stock_cost, account: @merchandise_inventory}],  
      employee_id: self.employee_id)
    elsif self.line_items.blank? && self.service_charges.present?
      Accounting::Entry.create(entry_type: "cash_order", order_id: self.id, commercial_document_id: self.customer_id, 
        commercial_document_type: self.customer.class, date: self.date, 
        description: "Repair / Maintenance for order ##{self.reference_number}", 
        debit_amounts_attributes: [{amount: self.repair_cost, account: @cash_on_hand}], 
        credit_amounts_attributes:[{amount: self.repair_cost, account: @repairs_and_maintenance}],  
        employee_id: self.employee_id)
    elsif self.line_items.present? && self.service_charges.present?
      Accounting::Entry.create(entry_type: "cash_order", order_id: self.id, commercial_document_id: self.customer_id, 
        commercial_document_type: self.customer.class, date: self.date, 
        description: "Sales, Repair / Maintenance for order ##{self.reference_number}", 
        debit_amounts_attributes: [{amount: self.stock_cost, account: @cost_of_goods_sold},
          {amount: self.total_amount, account: @cash_on_hand}], 
        credit_amounts_attributes:[{amount: self.stock_cost, account: @merchandise_inventory},
          {amount: self.items_amount, account: @sales},
          {amount: self.repair_cost, account: @repairs_and_maintenance}],  
        employee_id: self.employee_id)
    end
  end

  def entry_for_credit
    if self.line_items.present? && self.service_charges.blank?
      Accounting::Entry.create(entry_type: "cash_order", order_id: self.id, commercial_document_id: self.customer_id, 
      commercial_document_type: self.customer.class, date: self.date, 
      description: "Credit for order ##{self.reference_number}", 
      debit_amounts_attributes: [{amount: self.items_amount, account: @accounts_receivable}, 
        {amount: self.stock_cost, account: @cost_of_goods_sold}], 
      credit_amounts_attributes:[{amount: self.items_amount, account: @sales}, 
        {amount: self.stock_cost, account: @merchandise_inventory}],  
      employee_id: self.employee_id)
    elsif self.line_items.blank? && self.service_charges.present?
      Accounting::Entry.create(entry_type: "cash_order", order_id: self.id, commercial_document_id: self.customer_id, 
        commercial_document_type: self.customer.class, date: self.date, 
        description: "Credit on Repair/Maintenance of order ##{self.reference_number}", 
        debit_amounts_attributes: [{amount: self.repair_cost, account: @accounts_receivable}], 
        credit_amounts_attributes:[{amount: self.repair_cost, account: @repairs_and_maintenance}],  
        employee_id: self.employee_id)
    elsif self.line_items.present? && self.service_charges.present?
      Accounting::Entry.create(entry_type: "cash_order", order_id: self.id, commercial_document_id: self.customer_id, 
        commercial_document_type: self.customer.class, date: self.date, 
        description: "Credit of Sales, Repair/Maintenance of order ##{self.reference_number}", 
        debit_amounts_attributes: [{amount: self.stock_cost, account: @cost_of_goods_sold},
          {amount: self.total_amount, account: @accounts_receivable}], 
        credit_amounts_attributes:[{amount: self.stock_cost, account: @merchandise_inventory},
          {amount: self.items_amount, account: @sales},
          {amount: self.repair_cost, account: @repairs_and_maintenance}],  
        employee_id: self.employee_id)
    end
  end

  private

  def ensure_not_referenced_by_line_items
    errors[:base] << "Order still referenced by line items" if self.line_items.present?
    return false 
  end

  def set_date
    self.date ||= Time.zone.now
  end

  def set_customer
    if customer_id.nil?
      customer_id = Customer.find_by(first_name: 'Guest').id
    end
  end
end
