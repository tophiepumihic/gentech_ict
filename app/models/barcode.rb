class Barcode < ApplicationRecord
	require 'csv'
	
  belongs_to :product
  validates :code, presence: true, uniqueness: true

  def self.import(file)
    CSV.foreach(file.path, headers: true) do |row|
      barcode_hash = row.to_hash
      barcode = Barcode.where(id: barcode_hash['id'])

      if barcode.count == 1
        barcode.first.update_attributes(barcode_hash)
      else
        Barcode.create!(barcode_hash)
      end
    end
  end
end
