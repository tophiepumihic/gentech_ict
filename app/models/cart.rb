class Cart < ApplicationRecord
  belongs_to :employee
  has_many :line_items, dependent: :destroy
  has_many :service_charges, dependent: :destroy
  has_many :stocks, through: :line_items

  # def labor
  #   service_charges.where(charge_type: 0).all
  # end

  # def service_fee
  #   service_charges.where(charge_type: 1).all
  # end

  def items_and_repairs
    line_items + service_charges
  end

  def additional_charges_amount
    service_charges.sum(:total_cost)
  end

  def line_items_amount
    line_items.sum(:total_price)
  end

  def total_amount
    line_items_amount + additional_charges_amount
  end

  def add_line_item(line_item)
    if self.stocks.include?(line_item.stock)
      self.line_items.where(stock_id: line_item.stock.id).delete_all
      # replace with a single item
      self.line_items.create!(stock_id: line_item.stock.id, quantity: line_item.quantity, unit_price: line_item.unit_price, total_price: line_item.total_amount, user_id: self.employee_id)
    else
      self.line_items.create!(stock_id: line_item.stock.id, quantity: line_item.quantity, unit_price: line_item.unit_price, total_price: line_item.total_amount, user_id: self.employee_id)
    end
  end
end
