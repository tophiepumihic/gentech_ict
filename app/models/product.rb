class Product < ApplicationRecord
  include PublicActivity::Common
  include PgSearch
  require 'csv'

  enum status: [:available, :unavailable]
  pg_search_scope( :search_by_name, 
                    against: [:name_and_description, :name, :description],
                    :associated_against => {:barcodes => [:code]},
                    using: { tsearch: { prefix: true }} )

  belongs_to :category
  belongs_to :program
  has_many :barcodes
  has_many :stocks, dependent: :destroy
  has_many :refunds, through: :stocks
  has_many :line_items, through: :stocks
  has_many :orders, through: :line_items
  validates :name, :retail_price, :unit, presence: true

  before_destroy :ensure_not_referenced_by_any_line_item
  before_save :set_name_description
  before_update :set_name_description

  def self.import(file)
    CSV.foreach(file.path, headers: true) do |row|
      product_hash = row.to_hash
      product = Product.where(id: product_hash['id'])

      if product.count == 1
        product.first.update_attributes(product_hash)
      else
        Product.create!(product_hash)
      end
    end
  end

  def self.by_category(category)
    all.where(category: category)
  end
  
  def quantity
    stocks.sum(:quantity)
  end

  def returned_quantity
    stocks.sum{|s| s.refunds.sum(:quantity)}
  end

  def in_stock
    if stocks.sum(:quantity) <= 0
      0
    else
      stocks.sum(:quantity) - sold - returned_quantity
    end
  end

  def sold
    line_items.sum(:quantity)
  end

  def self.total_inventory_cost
    all.sum { |t| t.total_in_stock_cost}
  end

  def total_stock_cost
    self.stocks.purchased.sum(:total_cost)
  end

  def total_in_stock_cost
    stocks.sum {|s| (s.quantity-s.sold)*s.unit_cost}
  end

  def quantity_and_unit
    "#{quantity} #{unit}"
  end

  def name_description
    "#{name} #{description}"
  end

  def set_name_description
    if description.present?
      self.name_and_description = name_description.upcase
    else
      self.name_and_description = name.upcase
    end
  end

  def out_of_stock?
    in_stock.zero? || in_stock.negative? || stocks.blank?
  end

  def self.out_of_stock
    all.order(:name).select{ |a| a.out_of_stock?}
  end

  def low_stock?
    if stocks.present?
      quantity <= stock_alert_count && !out_of_stock?
    else
      false
    end
  end

  def self.low_stock
    all.order(:name).select{ |p| p.low_stock? }
  end

  def stock_alert
    if out_of_stock?
      "Out of Stock"
    elsif low_stock?
      "Low Stock"
    end
  end

  def set_status
    if !out_of_stock? || low_stock?
      self.available!
    elsif out_of_stock?
      self.unavailable!
    end
  end

  def update_prices
    self.stocks.each do |stock|
      stock.retail_price = self.retail_price
      stock.save
    end
  end
  private
  def ensure_not_referenced_by_any_line_item
    if line_items.empty?
      return true
    else
      errors.add(:base, 'Line Items present')
      return false
    end
  end
end
