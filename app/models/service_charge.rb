class ServiceCharge < ApplicationRecord
	belongs_to :cart
  belongs_to :order
  belongs_to :employee, foreign_key: 'user_id'
  belongs_to :customer, foreign_key: 'customer_id'
  validates :quantity, :unit_cost, :description, :device_description, presence: true
  before_save :set_total_charges
  # enum charge_type: [:labor, :service_fee]

  def total_charge
    unit_cost * quantity
  end

  def set_total_charges
    self.total_cost = total_charge
  end
end
