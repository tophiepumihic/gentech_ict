module Accounting
  class BalanceSheetPdf < Prawn::Document
    TABLE_WIDTHS = [50, 350, 152, 20]
    def initialize(equity, liabilities, assets, revenues, expenses, from_date, to_date, view_context)
      super(margin: 20, page_size: [612, 1008], page_layout: :portrait)
      @equities = equity
      @liabilities = liabilities
      @assets = assets
      @revenues = revenues
      @expenses = expenses
      @from_date = from_date
      @to_date = to_date
      @view_context = view_context
      balance_heading
      display_asset if @assets.present?
      display_liability if @liabilities.present?
      display_equity if @equities.present?
      display_balance
      # move_down 30
      # income_heading
      # display_revenue if @revenues.present?
      # display_expense if @expenses.present?
      # display_income
    end

    def total_equity
      if @equities.present?
        @equities.active_balance(from_date: @from_date.yesterday.end_of_day, to_date: @to_date.end_of_day)
      else
        0
      end
    end
    def total_asset
      if @assets.present?
        @assets.active_balance(from_date: @from_date.yesterday.end_of_day, to_date: @to_date.end_of_day)
      else
        0
      end
    end
    def total_liability
      if @liabilities.present?
        @liabilities.active_balance(from_date: @from_date.yesterday.end_of_day, to_date: @to_date.end_of_day)
      else
        0
      end
    end
    def total_revenue
      if @revenues.present?
        @revenues.active_balance(from_date: @from_date.yesterday.end_of_day, to_date: @to_date.end_of_day)
      else
        0
      end
    end
    def total_expense
      if @expenses.present?
        @expenses.active_balance(from_date: @from_date.yesterday.end_of_day, to_date: @to_date.end_of_day)
      else
        0
      end
    end

    def price(number)
      @view_context.number_to_currency(number, :unit => "P ")
    end

    def balance_heading
      text "#{Business.last.name}", style: :bold, size: 11, align: :center
      text "#{Business.last.address}", size: 10, align: :center
      move_down 15
      text 'BALANCE SHEET', size: 11, align: :center, style: :bold
      text "#{@from_date.strftime("%B %e, %Y")} - #{@to_date.strftime("%B %e, %Y")}", size: 10, align: :center
      move_down 5
      stroke_horizontal_rule
    end
    def income_heading
      text 'INCOME STATEMENT', size: 11, align: :center, style: :bold
      text "#{@from_date.strftime("%B %e, %Y")} - #{@to_date.strftime("%B %e, %Y")}", size: 10, align: :center
      move_down 5
      stroke_horizontal_rule
    end

    def display_asset
      move_down 10
      text "ASSETS", size: 11
      stroke_horizontal_rule
      header = ["", "", "", ""]
      footer = ["", "<b>SUB-TOTAL</b>", "<b>#{price(total_asset)}</b>", ""]
      assets_data ||= Accounting::Asset.active_with_balance.map { |e| ["", e.name, price(e.balance(from_date: @from_date, to_date: @to_date)), ""]}
      table_data = [header, *assets_data, footer]
      table(table_data, cell_style: { size: 9, font: "Helvetica", inline_format: true, :padding => [2, 4, 2, 4]}, column_widths: TABLE_WIDTHS) do
        cells.borders = [:top]
        row(0).font_style = :bold
        column(2).align = :right
      end
    end

    #################################################################################

    def display_liability
      move_down 10
      text "LIABILITIES", size: 11
      stroke_horizontal_rule
      header = ["", "", "", ""]
      footer = ["", "<b>SUB-TOTAL</b>", "<b>#{price(total_liability)}</b>", ""]
      assets_data ||= Accounting::Liability.active_with_balance.map { |e| ["", e.name, price(e.balance(from_date: @from_date, to_date: @to_date)), ""]}
      table_data = [header, *assets_data, footer]
      table(table_data, cell_style: { size: 9, font: "Helvetica", inline_format: true, :padding => [2, 4, 2, 4]}, column_widths: TABLE_WIDTHS) do
        cells.borders = [:top]
        row(0).font_style = :bold
        column(2).align = :right
      end
    end

    ########################################################################

    def display_equity
      move_down 10
      text "EQUITY", size: 11
      stroke_horizontal_rule
      header = ["", "", "", ""]
      footer = ["", "<b>SUB-TOTAL</b>", "<b>#{price(total_equity)}</b>", ""]
      assets_data ||= Accounting::Equity.active_with_balance.map { |e| ["", e.name, price(e.balance(from_date: @from_date, to_date: @to_date)), ""]}
      table_data = [header, *assets_data, footer]
      table(table_data, cell_style: { size: 9, font: "Helvetica", inline_format: true, :padding => [2, 4, 2, 4]}, column_widths: TABLE_WIDTHS) do
        cells.borders = [:top]
        row(0).font_style = :bold
        column(2).align = :right
      end
    end

    def display_balance
      move_down 5
      stroke_horizontal_rule
      move_down 2
      stroke_horizontal_rule
      table(total_balance_data, cell_style: { :padding => [2,4,4,2], size: 10, font: "Helvetica", inline_format: true}, column_widths: TABLE_WIDTHS) do
        cells.borders = []
        column(2).align = :right
      end
    end

    def total_balance_data
      @total_balance_data ||=  [["", "<b>TOTAL</b>", "<b>#{price(total_asset + total_liability + total_equity)}</b>", ""]]
    end

    ##############################################################################

    def display_revenue
      move_down 10
      text "REVENUES", size: 11
      stroke_horizontal_rule
      header = ["", "", "", ""]
      footer = ["", "<b>SUBTOTAL</b>", "<b>#{price(total_revenue)}</b>", ""]
      assets_data ||= Accounting::Revenue.active_with_balance.map { |e| ["", e.name, price(e.balance(from_date: @from_date, to_date: @to_date)), ""]}
      table_data = [header, *assets_data, footer]
      table(table_data, cell_style: { size: 9, font: "Helvetica", inline_format: true, :padding => [2, 4, 2, 4]}, column_widths: TABLE_WIDTHS) do
        cells.borders = [:top]
        row(0).font_style = :bold
        column(2).align = :right
      end
    end

    def display_expense
      move_down 10
      text "EXPENSES", size: 11
      stroke_horizontal_rule
      header = ["", "", "", ""]
      footer = ["", "<b>SUBTOTAL</b>", "<b>#{price(total_expense)}</b>", ""]
      assets_data ||= Accounting::Expense.active_with_balance.map { |e| ["", e.name, price(e.balance(from_date: @from_date, to_date: @to_date)), ""]}
      table_data = [header, *assets_data, footer]
      table(table_data, cell_style: { size: 9, font: "Helvetica", inline_format: true, :padding => [2, 4, 2, 4]}, column_widths: TABLE_WIDTHS) do
        cells.borders = [:top]
        row(0).font_style = :bold
        column(2).align = :right
      end
    end

    def display_income
      move_down 5
      stroke_horizontal_rule
      move_down 2
      stroke_horizontal_rule
      table(total_income_data, cell_style: { :padding => [2,4,4,2], size: 10, font: "Helvetica", inline_format: true}, column_widths: TABLE_WIDTHS) do
        cells.borders = []
        column(2).align = :right
      end
    end

    def total_income_data
      @total_income_data ||=  [["", "<b>TOTAL</b>", "<b>#{price(total_revenue - total_expense)}</b>", ""]]
    end


  end
end
