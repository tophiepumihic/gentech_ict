module Customers
  class PaymentsPdf < Prawn::Document
    TABLE_WIDTHS = [90, 160, 70, 70, 70, 72]
    HEADING_WIDTHS = [200,100,100,100]
    def initialize(member, from_date, to_date, view_context)
      super(margin: 40, page_size: [612, 1008], page_layout: :portrait)
      @member = member
      @from_date = from_date
      @to_date = to_date
      @payment_entries = @member.payment_entries.created_between({from_date: @from_date, to_date: @to_date.end_of_day}).order(:date).all
      @view_context = view_context
      heading
      payments_table
    end
    def price(number)
      @view_context.number_to_currency(number, :unit => "P ")
    end
    def heading
      text "#{Business.last.try(:name)}", size: 11, align: :center, style: :bold
      text "#{Business.last.address}", size: 9, align: :center
      text "TIN: #{Business.last.try(:tin)}", size: 9, align: :center
      text "#{Business.last.try(:mobile_number)}  #{Business.last.try(:email)}", size: 8, align: :center
      move_down 10
      text "CUSTOMER PAYMENTS REPORT", size: 11, align: :center, style: :bold
      move_down 1
      if @from_date.strftime('%B %e, %Y') == @to_date.strftime('%B %e, %Y')
        text "#{@from_date.strftime('%B %e, %Y')}", align: :center, size: 10
      else
        text "#{@from_date.strftime('%B %e, %Y')} - #{@to_date.strftime('%B %e, %Y')}", align: :center, size: 10
      end
      stroke_horizontal_rule
      move_down 5
      text "SUMMARY", style: :bold, size: 10
      table(heading_data, header: true, cell_style: { size: 10, font: "Helvetica", :padding => [3,3,0,0]}, column_widths: HEADING_WIDTHS) do
        cells.borders = []
      end
      move_down 10
      stroke_horizontal_rule
    end
    def heading_data
      [["#{@member.full_name}", "", "Principal:", "#{price(@member.total_credit_transactions)}" ]] +
      [["#{@member.address_details || "Address: N/A"}", "", "Total Interest:", "#{price(@member.total_interest)}" ]] +
      [["#{@member.mobile}", "", "Total Payment:", "#{price(@member.total_payment)}" ]] +
      [["", "", "Remaining Balance:", "#{price(@member.total_remaining_balance)}" ]]

    end

    def payments_table
      if @payment_entries.blank?
        move_down 10
        text "No payments data.", align: :center
      else
        move_down 10
        header = [["DATE", "REFERENCE NUMBER", "PAYMENT", "INTEREST", "DISCOUNT", "TOTAL"]]
        table(header, :cell_style => {size: 9, :padding => [2, 4, 2, 4]}, column_widths: TABLE_WIDTHS) do
          cells.borders = []
          row(0).font_style = :bold
          column(2).align = :right
          column(3).align = :right
          column(4).align = :right
          column(5).align = :right
        end
        stroke_horizontal_rule
        header = ["", "", "", "", "", ""]
        footer = ["", "", "", "", "", ""]
        payments_data = @payment_entries.map { |e| [ 
          e.date.strftime("%B %e, %Y"), 
          e.reference_number, 
          price(e.credit_amounts.first.amount),
          price(e.credit_amounts.second.amount),
          price(e.credit_amounts.third.amount),
          price(e.credit_amounts.first.amount + (e.credit_amounts.second.amount - e.credit_amounts.third.amount))
          ]}
        table_data = [header, *payments_data, footer]
        table(table_data, cell_style: { size: 9, font: "Helvetica", inline_format: true, :padding => [2, 4, 2, 4]}, column_widths: TABLE_WIDTHS) do
          cells.borders = [:top]
          row(0).font_style = :bold
          column(2).align = :right
          column(3).align = :right
          column(4).align = :right
          column(5).align = :right
        end
      end
    end
  end
end
